import * as jwt from 'jsonwebtoken'
import { isDummyGeneratorActive } from '../databasedummies/dummies'

export const checkAuth = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1] // send token in header Authorization in format: Bearer *token*
        jwt.verify(token, process.env.TOKEN_SECRET)
        next()
    } catch (error) {
        if (
            process.env.NODE_ENV !==
            'production' /* && isDummyGeneratorActive */
        ) {
            return next()
        }
        res.status(401).json({ message: 'Not authenticated!' })
    }
}
