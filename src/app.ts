import express from 'express'
import { connectMongoDB, generateDefaultOrder } from './database'
import {
    addSnack,
    addSnacksfromList,
    getSnack
} from './controllers/snackController'
import { allSnacks } from './controllers/snackController'
import {
    getQuestion,
    allQuestions,
    addAllQuestions
} from './controllers/questionController'
import {
    addActivitiesfromList,
    addActivity,
    allActivity,
    getActivity,
    updateActivity
} from './controllers/activitiesController'
import {
    addSport,
    updateSportsfromLists,
    allSports
} from './controllers/sportController'
import logger from './logger'
import swaggerJsDoc from 'swagger-jsdoc'
import * as swaggerUi from 'swagger-ui-express'
import { signinController, signupController } from './routes/auth'
import { FrontendContainer } from './controllers/FrontendController'
import { Server } from 'http'
import { AddressInfo } from 'net'
import dotenv from 'dotenv'
import {
    deleteImageController,
    getAllImagesController,
    getImageController,
    postImageController
} from './controllers/imagesController'
import {
    addSettings,
    getallSettings,
    updateSettings
} from './controllers/settingsController'
import cors = require('cors')
import { postViewTrackingController } from './controllers/trackingController'
import { createDummieDatabase } from './databasedummies/dummies'
import {
    getOrder,
    updateOrder,
    addOrdered
} from './controllers/orderedController'
import { calcMatchingSportsController } from './routes/sportsScore'
import { checkAuth } from './middleware/check-auth-admin'
import {
    addItemsfromSyncToDB,
    getNewSyncController
} from './controllers/syncController'
import * as path from 'path'
import { stat } from 'fs'
import { AdminModel } from './database/admin'
import * as bcrypt from 'bcrypt'

// load environment config from .env file
dotenv.config()

logger.info('===========================')
logger.info(
    `server is running in "${
        process.env.NODE_ENV === 'production' ? 'prod' : 'dev'
    }"`
)
logger.info('===========================')

if (!process.env.TOKEN_SECRET) {
    logger.error(
        'Missing TOKEN_SECRET env var. Please specify it in .env file or via environment.'
    )
    process.exit()
}

// Our Express APP config
const app = express()
app.set('port', process.env.APP_PORT || 5000)

app.use(express.urlencoded({ extended: false })) // Parses urlencoded bodies
app.use(express.json()) // Send JSON responses

if (process.env.NODE_ENV !== 'production') {
    const swaggerOptions = {
        swaggerDefinition: {
            info: {
                version: '1.0.0',
                title: 'Uni-Sport-O-Mat API',
                description: 'API Information',
                servers: ['http://localhost:5000']
            }
        },
        // ['.routes/*.ts']
        apis: ['**/*.ts']
    }
    const swaggerDocs = swaggerJsDoc(swaggerOptions)
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))
    logger.info('NODE_ENV is not production, running swagger in /api-docs')
}

stat('./frontend-build', (e, stats) => {
    if (e || !stats.isDirectory()) {
        logger.warn(
            "Folder ./frontend-build doesn't exist. To have a monolithic app, build frontend with npm run build and copy contents of ./build into the folder ./frontend-build!"
        )
    }
})

// logs all requests on http level => could later be saved into "access.log" for example
app.use((req, res, next) => {
    logger.http(req.url)
    const start = Date.now()
    res.on('finish', () => {
        logger.http(
            `Finished ${req.url} | ${res.statusCode} [${Date.now() - start}ms]`
        )
    })
    next()
})
// // API Endpoints

app.post('/api/question', checkAuth, addAllQuestions)
app.get('/api/question', checkAuth, allQuestions)
app.get('/api/question/:id', checkAuth, getQuestion)

app.post('/api/activity', checkAuth, addActivity)
app.get('/api/activity', allActivity)
app.get('/api/activity/:id', getActivity)
app.post('/api/activities', checkAuth, addActivitiesfromList)

app.post('/api/snack', checkAuth, addSnack)
app.get('/api/snacks', allSnacks)
app.get('/api/snacks/:id', getSnack)
app.post('/api/snacks', checkAuth, addSnacksfromList)

app.post('/api/sport', checkAuth, addSport)
app.get('/api/sport', allSports)

app.post('/api/ordered/:id', checkAuth, updateOrder)
app.post('/api/ordered', checkAuth, addOrdered)
app.get('/api/ordered', getOrder)
app.post('/api/sports', checkAuth, updateSportsfromLists)

app.get('/api/frontendContainer', FrontendContainer)

app.post('/api/get-matching-sports', calcMatchingSportsController)

app.post('/api/auth/signin', signinController)
app.post('/api/auth/signup', checkAuth, signupController)

app.post('/api/nextStep', postViewTrackingController)

app.use(
    '/api/image',
    express.raw({
        limit: '4MB',
        type: 'image/*'
    })
)
app.post('/api/image', checkAuth, postImageController)
app.get('/api/image', getAllImagesController)
app.get('/api/image/:imgId.:imgType', getImageController)
app.delete('/api/image/:imgId.:imgType', checkAuth, deleteImageController)

app.get('/api/sync', getNewSyncController)
app.post('/api/sync', checkAuth, addItemsfromSyncToDB)

app.post('/api/settings', checkAuth, addSettings)
app.get('/api/settings', getallSettings)
app.post('/api/settings/:id', checkAuth, updateSettings)

app.use('/', express.static('frontend-build'))

app.get('*', (req, res) => {
    res.sendFile(path.resolve('./', 'frontend-build', 'index.html'))
})

async function startExpress(): Promise<Server> {
    return new Promise((resolve, reject) => {
        const server = app
            .listen(app.get('port'), () => {
                resolve(server)
            })
            .on('error', reject)
    })
}

// main startup
;(async () => {
    const [server, _] = await Promise.all([startExpress(), connectMongoDB()])
    const addr = server.address() as AddressInfo
    logger.info(`Server started on ${JSON.stringify(addr)}`)

    // generate default admin login
    const loginIdx = process.argv.indexOf('--create-admin')
    if (loginIdx > -1) {
        const login = process.argv[loginIdx + 1]
        const password = process.argv[loginIdx + 2]
        if (!login || !password) {
            logger.error('Missing login or password after param --login')
            process.exit()
        }
        const hash = await bcrypt.hash(password, 10)
        const user = new AdminModel({
            username: login,
            password: hash,
            active: true
        })

        await user.save()

        logger.info(`Created admin user ${login}`)
    }

    if (
        process.env.NODE_ENV !== 'production' &&
        process.argv.includes('--gen-data')
    ) {
        // creating database dummies
        await createDummieDatabase()
        logger.info(
            'generated dummie data (only done in dev env, if not wanted, run node in production)'
        )
    } else if (process.env.NODE_ENV !== 'production') {
        logger.info(
            'node started in development mode. To generate dummie database entries, start with "npm run start -- --gen-data"'
        )
    }

    // generate default order if not existing
    generateDefaultOrder()
})()

export default app
