import mongoose, { Model, ObjectId } from 'mongoose'
import { Request, Response } from 'express'
import { OrderedModel } from '../database/ordered'
import { SnacksModel } from '../database/snack'
import { ActivityModel } from '../database/activity'

export let addOrdered = async (req: Request, res: Response) => {
    var ordered = new OrderedModel(req.body)
    var number = 0

    var arrayIterationReqBody = req.body.order.length
    var arrayNotExistingIds = []

    for (var i = 0; i < arrayIterationReqBody; i++) {
        //console.log('search for: ' + req.body.order[i])
        //Boolean found
        var found = new Boolean(false)

        var temp = req.body.order[i]
        console.log('temp: ' + temp)

        if (mongoose.Types.ObjectId.isValid(temp.toString())) {
            //check if ObjectId is existing
            await ActivityModel.findOne(
                { _id: req.body.order[i].toString() },
                function (err, activity) {
                    if (activity == null) {
                        console.log('Not found in activities' + temp)
                    } else {
                        console.log('Found in activities' + temp)
                        found = true
                    }
                }
            )
            if (found == true) {
                continue
            }

            await SnacksModel.findOne(
                { _id: req.body.order[i].toString() },
                function (err, Snacks) {
                    if (Snacks == null) {
                        //When not found here id dosent exist in snacks or activity
                        arrayNotExistingIds.push(temp)
                        console.log('Not found in snacks err' + temp)
                    } else {
                        console.log('Found in snacks' + Snacks._id)
                    }
                }
            )
        } else {
            arrayNotExistingIds.push(temp)
        }
    }

    if (arrayNotExistingIds.length == 0) {
        OrderedModel.countDocuments({}, function (err, count) {
            if (count >= 1) {
                res.status(400).send(
                    'An Order is already existing. Just one Order can be created'
                )
            } else {
                ordered.save((err: any) => {
                    if (err) {
                        res.status(400).send(err)
                    } else {
                        res.send(ordered)
                    }
                })
            }
        })
    } else {
        res.status(400).send(
            res.send(
                'The following Ids are not existing in DB:' +
                    arrayNotExistingIds.toString()
            )
        )
    }
}

// get /ordered  # returns all orders
export let getOrder = (req: Request, res: Response) => {
    console.log(req.body)
    let ordered = OrderedModel.find((err: any, ordered: any) => {
        if (err) {
            res.status(400).send('Error!')
        } else {
            res.send(ordered)
        }
    })
}

export let updateOrder = async (req: Request, res: Response) => {
    var number = 0

    var arrayIterationReqBody = req.body.order.length
    var arrayNotExistingIds = []

    for (var i = 0; i < arrayIterationReqBody; i++) {
        //console.log('search for: ' + req.body.order[i])
        //Boolean found
        var found = new Boolean(false)

        var temp = req.body.order[i]
        console.log('temp: ' + temp)

        if (mongoose.Types.ObjectId.isValid(temp.toString())) {
            //check if ObjectId is existing
            await ActivityModel.findOne(
                { _id: req.body.order[i].toString() },
                function (err, activity) {
                    if (activity == null) {
                        console.log('Not found in activities' + temp)
                    } else {
                        console.log('Found in activities' + temp)
                        found = true
                    }
                }
            )
            if (found == true) {
                continue
            }

            await SnacksModel.findOne(
                { _id: req.body.order[i].toString() },
                function (err, Snacks) {
                    if (Snacks == null) {
                        //When not found here id dosent exist in snacks or activity
                        arrayNotExistingIds.push(temp)
                        console.log('Not found in snacks err' + temp)
                    } else {
                        console.log('Found in snacks' + Snacks._id)
                    }
                }
            )
        } else {
            arrayNotExistingIds.push(temp)
        }
    }

    //check if array is empty
    console.log('Array length: ' + arrayNotExistingIds.length)
    if (arrayNotExistingIds.length == 0) {
        console.log('drinne ')
        await OrderedModel.findByIdAndUpdate(
            req.params.id,
            req.body,
            (err: any, Ordered: any) => {
                if (err) {
                    res.status(400).send(err)
                } else {
                    res.send('Successfully updated Ordered!')
                }
            }
        )
    } else {
        res.status(400).send(
            res.send(
                'The following Ids are not existing in DB:' +
                    arrayNotExistingIds.toString()
            )
        )
    }

    //console.log('array: ' + arrayNotExistingIds.toString())
}

export let deleteSettings = (req: Request, res: Response) => {
    OrderedModel.deleteOne({ _index: req.params.index }, (err: any) => {
        if (err) {
            res.status(400).send('Error!')
        } else {
            res.send('success')
        }
    })
}
