import { Request, Response } from 'express'
import {
    AcceptImageHeader,
    IImage,
    ImageModel,
    ImageTypeToMime,
    MimeToImageType,
    toUrl
} from '../database/image'
import logger from '../logger'
import LRUCache from 'lru-cache'
import { fromBuffer } from 'detect-file-type'
import { ImagesInformationResponse } from '../types/ApiTypes'
import { ActivityModel } from '../database/activity'
import { SnacksModel } from '../database/snack'
import { SettingsModel } from '../database/settings'
import { json } from 'body-parser'

async function getMimeType(buffer: Buffer): Promise<string | undefined> {
    return new Promise((resolve, reject) => {
        fromBuffer(buffer, (err, res) => {
            if (err) reject(err)
            if (!res.mime) reject('mime not on fromBuffer res')
            resolve(res.mime)
        })
    })
}

// example test command with curl:  curl -H "Content-Type: image/jpeg" --data-binary @IMG_20210305_183233.jpg -v http://192.168.178.52:5000/api/image

/**
 * @swagger
 * /api/image:
 *   post:
 *     description: post an image, and get the matching id back
 */
export const postImageController = async (req: Request, res: Response) => {
    if (!MimeToImageType[req.headers['content-type']]) {
        logger.warn(
            `Admin tried to upload image with mime type "${req.headers['content-type']}"`
        )
        res.status(415) // unsupported media type
        res.setHeader('Accept', AcceptImageHeader)
        res.end()
        return
    }

    if (req.headers.expect === '100-continue') {
        // allow client to send more data
        // TODO: is this even necessary? curl waits for this but also says "We are completely uploaded and fine"
        res.writeContinue()
    }

    // check if body contains valid image and size is < 4mb and is valid image type

    if (!(req.body instanceof Buffer)) {
        logger.error(
            'image upload: Client send req with valid mime type, but body was not parsed as raw data?'
        )
        res.status(500)
        res.json({
            error: 'req.body was not parsed as Buffer. Check backend log'
        })
        res.end()
        return
    }

    let detectedMimeType
    try {
        detectedMimeType = await getMimeType(req.body)
    } catch (error) {
        logger.error(`image upload: ${error}`)
        res.status(500)
        res.end()
        return
    }

    if (detectedMimeType !== req.headers['content-type']) {
        logger.error(
            `image upload: header conent type = ${req.headers['content-type']}, detected ${detectedMimeType}`
        )
        res.status(422)
        res.json({
            error: `http content-type diverged from detected content-type ${detectedMimeType}`
        })
        res.end()
        return
    }

    if (req.body.length > 4000000) {
        res.status(413)
        res.json({
            error: `Max image size 4MB, posted image had ${
                req.body.length / 1000000
            }MB`
        })
        res.end()
        return
    }

    // store image into database

    try {
        const imageEntry = new ImageModel({
            data: req.body,
            imgType: MimeToImageType[detectedMimeType],
            uploadDate: new Date()
        })

        const savedImage = await imageEntry.save()

        res.status(200)
        res.json({
            url: toUrl(savedImage)
        })
        res.end()
    } catch (error) {
        logger.error('image upload: ' + error)
        res.status(500)
        res.json({
            error: 'Failed to write image to database'
        })
    }
}

// stores the last 20 served images to prevent database hits each time
const imageCache = new LRUCache<string, IImage>({
    max: 20
})

export const getImageController = async (req: Request, res: Response) => {
    if (!req.params.imgId || !req.params.imgType) {
        res.status(400)
        res.type('html')
        res.send('<h1>path has wrong format</h1>')
        res.end()
        return
    }

    const imgId = req.params.imgId
    const imgType = req.params.imgType
    let image: IImage = imageCache.get(imgId)
    if (!image) {
        try {
            image = await ImageModel.findById(imgId).exec()
            if (image) {
                imageCache.set(imgId, image)
            } else {
                res.status(404)
                res.type('html')
                res.send("<h1>Image doesn't exist :(")
                res.end()
                return
            }
        } catch (error) {
            res.status(404)
            res.type('html')
            res.send("<h1>Image doesn't exist :(")
            res.end()
            return
        }
    } else {
        console.log('image was cached')
    }

    if (image.imgType !== imgType || !ImageTypeToMime[image.imgType]) {
        res.status(404)
        res.type('html')
        res.send(
            `<h1>Image was found, but with diffrent ".${image.imgType}" type`
        )
        res.end()
        return
    }
    res.status(200)
    // Caching allowed by proxys etc and for 2 hours
    res.set('Cache-control', 'public, max-age=7200')
    res.type(ImageTypeToMime[image.imgType])
    res.send(image.data)
    res.end()
}

/**
 * get /api/image
 * lists all image metadata for image managing
 * returns {@link ImagesInformationResponse}
 */
export const getAllImagesController = async (req: Request, res: Response) => {
    // get only _id, imgType and upload date
    const allImages = await ImageModel.find(
        {},
        { imgType: 1, uploadDate: 1 }
    ).exec()

    if (!allImages) {
        logger.error('Failed to get any images??')
        res.status(500)
        res.end()
        return
    }

    // get current image count from all locations where images are used
    const allImagesMap: {
        [id: string]: IImage & { count: number }
    } = allImages.reduce((acc, val) => {
        const url = toUrl(val)
        acc[url] = val
        acc[url].count = 0
        return acc
    }, {})

    const [activities, snacks, settings] = await Promise.all([
        ActivityModel.find({}, { imageUrl: 1 }),
        SnacksModel.find({}, { imageUrl: 1 }),
        SettingsModel.findOne({}, { imageUrl: 1 })
    ])

    for (const entry of [...activities, ...snacks, settings].filter(
        (e) => !!e
    )) {
        if (entry.imageUrl && allImagesMap[entry.imageUrl]) {
            allImagesMap[entry.imageUrl].count += 1
        }
    }

    res.status(200)
    res.json(
        Object.entries(allImagesMap).map(([url, val]) => {
            return {
                url,
                uploaded: val.uploadDate.toString(),
                activeUsages: val.count
            }
        }) as ImagesInformationResponse
    )
}

/**
 * DELETE /api/image/:imgId.:imgType
 */
export const deleteImageController = async (req: Request, res: Response) => {
    if (!req.params.imgId || !req.params.imgType) {
        res.status(400)
        res.type('html')
        res.send('<h1>path has wrong format</h1>')
        res.end()
        return
    }

    const imgId = req.params.imgId
    const imgType = req.params.imgType

    const image = await ImageModel.findById(imgId).exec()

    if (image.imgType !== imgType) {
        res.status(400)
        res.json({
            error: `only image with type .${image.imgType} exists`
        })
        res.end()
        return
    }

    await image.remove()
    logger.info(`Image ${toUrl(image)} was removed`)
    res.status(200)
    res.end()
}
