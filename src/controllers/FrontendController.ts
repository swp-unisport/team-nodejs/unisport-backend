import { ActivityModel, IActivity } from '../database/activity'
import { IQuestion, QuestionModel } from '../database/question'
import { ISnacks, SnacksModel, SnacksSchema } from '../database/snack'
import { Request, Response } from 'express'
import { Page } from '../types/ApiTypes'
import { OrderedModel } from '../database/ordered'
import { ObjectId } from 'mongoose'
import logger from '../logger'

/**
 * @swagger
 * /api/frontendContainer/:
 *   get:
 *     summary: get data for displaying user "quiz"
 *              order is implicit by ordering of Pages
 */
export let FrontendContainer = async (req: Request, res: Response) => {
    let Snacks = await SnacksModel.find({ active: true }).exec()
    console.log('snacks finished')
    let Questions = await QuestionModel.find({ active: true }).exec()
    console.log('qeustions finished')
    let Activities = await ActivityModel.find({ active: true }).exec()
    console.log('snacks finished')
    const allPages = new Map<string, Page>()

    Snacks.forEach((snack) =>
        allPages.set(snack.id, {
            ...(snack as any)._doc,
            id: snack.id,
            type: 'snack'
        })
    )
    Questions.forEach((q) =>
        allPages.set(q.id, { ...(q as any)._doc, id: q.id, type: 'question' })
    )
    Activities.forEach((a) =>
        allPages.set(a.id, { ...(a as any)._doc, id: a.id, type: 'activity' })
    )

    const orderData = await OrderedModel.findOne().exec()

    if (!orderData?.order) {
        logger.error('no data for OrderedModel.findOne() found!')
        res.status(500)
        res.end()
        return
    }

    let currentOrder = orderData.order

    let result: Page[] = []
    for (const viewId of currentOrder) {
        const view = allPages.get(viewId.toString())
        if (!view) {
            logger.error(
                `View ${viewId} was set in order but doesn't exist in pages`
            )
        } else {
            result.push(view)
        }
    }

    res.status(200)
    res.json(result)

    //imageUrl: 'http://placekitten.com/200/300',
    //information: 'BeinBewegen',
    //index: 2,
    //id: 60bf8c298e2e9542787add70
}

//1. start und endseite ?
//2.  Order ?? -> im Moment nimmt er 2 zufällige Snacks
//      2 zufällige activities und 6 zufällige Questions
//3.
