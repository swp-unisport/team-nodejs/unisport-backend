import { Request, Response } from 'express'
import { SettingsModel, SettingsSchema } from '../database/settings'

// post /settings  # adds a snack
export let addSettings = (req: Request, res: Response) => {
    var Setting = new SettingsModel(req.body)

    Setting.save((err: any) => {
        if (err) {
            res.status(400)
            res.send(err)
        } else {
            res.send(Setting)
        }
    })
}

// get /allsettings  # returns all snacks
export let getallSettings = (req: Request, res: Response) => {
    let Snacks = SettingsModel.find((err: any, Snacks: any) => {
        if (err) {
            res.status(400)
            res.send('Error!')
        } else {
            res.send(Snacks)
        }
    })
}

// get /settings/{index} # returns snack with id index
export let getSettings = (req: Request, res: Response) => {
    SettingsModel.findById(req.params.id, (err: any, Settings: any) => {
        if (err) {
            res.status(400)
            res.send('Error!')
        } else {
            res.send(Settings)
        }
    })
}

// delete /settings/{index}
export let deleteSettings = (req: Request, res: Response) => {
    SettingsModel.deleteOne({ _index: req.params.index }, (err: any) => {
        if (err) {
            res.status(400)
            res.send('Error!')
        } else {
            res.send('success')
        }
    })
}
// update /updatesettings
export let updateSettings = (req: Request, res: Response) => {
    console.log(req.body)
    let snack = SettingsModel.findByIdAndUpdate(
        req.params.id,
        req.body,
        (err: any, Settings: any) => {
            if (err) {
                res.status(400)
                res.send(err)
            } else {
                res.send('Successfully updated Settings!')
            }
        }
    )
}
