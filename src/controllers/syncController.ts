import { Request, Response } from 'express'
import { ISports, SportModel } from '../database/sport'
import logger from '../logger'
import { DEFAULT_URL, parseFromBuchsys } from '../synchronize'

/**
 * uses DEFAULT_URL from synchronize.ts per default, can be overwritten with query_url=... as url param
 *
 * !!!
 * Currently, we only match existing sports by name.
 * An improvement would be to also match by url to catch
 * slight name changes (if buchsys keeps url on name change)
 */
export const getNewSyncController = async (req: Request, res: Response) => {
    const url = req.params.query_url ?? DEFAULT_URL
    logger.info(`Starting new sync with url ${url}`)
    let newSportsState
    try {
        newSportsState = await parseFromBuchsys(url)
    } catch (error) {
        logger.error(
            'Failed to fetch & parse new sports list: ' + JSON.stringify(error)
        )
        res.status(500)
        res.json({
            error: error
        })
        res.end()
        return
    }

    const newSportsMap: Map<String, [string, string]> = newSportsState.reduce(
        (map, sport) => map.set(sport[0], sport),
        new Map()
    )

    const currentSportsState: ISports[] = await SportModel.find({}).exec()
    // for faster access: map sport name to ISports
    const currentSportsMap: Map<string, ISports> = currentSportsState.reduce(
        (map, sport) => map.set(sport.name, sport),
        new Map()
    )

    /*
    We need to divide list of `newSportsState` into three sets:
    - new added sports
    - recurring sports
    - removed sports
    */
    const newSports: { name: string; url: string }[] = []
    const recurringSports: {
        name: string
        url: string
        lastActive: boolean
    }[] = []
    const removedSports: { name: string; url: string }[] = []

    for (const [sportName, sportUrl] of newSportsState) {
        const maybeOldSportEntry = currentSportsMap.get(sportName)
        if (!maybeOldSportEntry) {
            newSports.push({ name: sportName, url: sportUrl })
        } else {
            recurringSports.push({
                name: sportName,
                url: sportUrl,
                lastActive: maybeOldSportEntry.active
            })
        }
    }

    for (const oldSport of currentSportsState) {
        const maybeNewSportEntry = newSportsMap.get(oldSport.name)
        if (!maybeNewSportEntry) {
            removedSports.push({ name: oldSport.name, url: oldSport.url })
        }
    }

    res.status(200)
    res.json({
        newSports,
        recurringSports,
        removedSports
    })
}

export let addItemsfromSyncToDB = async (req: Request, res: Response) => {
    //{toAdd:[.........],toRemove:[.........],toActive:[.........],toInactive:[.........]}
    // each element in list ist {title:... , url:..}

    const categoryWeights = new Map()

    //fix if first entries in db
    try {
        let categoryWeightsDB = (await SportModel.findOne().lean().exec())
            .categoryWeights

        for (let category in categoryWeightsDB) {
            // {{sportart1:0} , {sportart2:0},{sportart3:1} , {sportart4:2},{sportart5:1} , {sportart6:2}}
            categoryWeights.set(category, undefined)
        }
    } catch {
        console.log('no categorieWeights exist in the database so far')
    }

    for (const sport of req.body.toAdd) {
        await SportModel.create({
            name: sport.name,
            url: sport.url,
            categoryWeights: categoryWeights,
            totalClicks: 0,
            totalRecommended: 0,
            active: true //?
        })
            .then(() => {
                console.log('added' + sport.name + ' to db ')
            })
            .catch(() => {
                res.send('Failed adding Sports from toAdd to DB')
            })
    }

    for (const sport of req.body.toRemove) {
        SportModel.deleteOne({ name: sport.name }, (err: any) => {
            if (err) {
                res.send('Error!')
            } else {
                console.log('succesfully deleted Sports from toRemove')
            }
        })
    }
    for (const sport of req.body.toActive) {
        await SportModel.findOneAndUpdate(
            { name: sport.name },
            {
                $set: {
                    active: true
                }
            },
            { new: true }
        )
            .then(() => {
                console.log('moved ' + sport.name + ' to active')
            })
            .catch(() => {
                return res.send('failed to move' + sport.name + 'to active')
            })
    }

    for (const sport of req.body.toInactive) {
        await SportModel.findOneAndUpdate(
            { name: sport.name },
            {
                $set: {
                    active: false
                }
            },
            { new: true }
        )
            .then(() => {
                console.log('moved ' + sport.name + ' to inactive')
            })
            .catch(() => {
                return res.send('failed to move' + sport.name + 'to inactive')
            })
    }
    res.status(200).send('all Sports added successfully')
}
