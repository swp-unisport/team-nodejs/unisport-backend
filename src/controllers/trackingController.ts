import { resolveNaptr } from 'dns'
import { Request, Response } from 'express'
import { ActivityModel } from '../database/activity'
import { QuestionModel } from '../database/question'
import { SnacksModel } from '../database/snack'
import logger from '../logger'
import { ViewTrackingPostBody } from '../types/ApiTypes'

/**
 * @swagger
 * /api/nextStep:
 *   post:
 *     description: post data on view change
 */
export const postViewTrackingController = async (
    req: Request,
    res: Response
) => {
    if (
        typeof req.body !== 'object' ||
        !('viewId' in req.body) ||
        !('timeOnPage' in req.body)
    ) {
        res.status(400)
        res.json({
            error: 'viewId or timeOnpage was missing in request'
        })
        res.end()
        return
    }

    const body = req.body as ViewTrackingPostBody

    // increase counter of visits of the page
    const matchingViews = await Promise.all([
        ActivityModel.findById(body.viewId),
        QuestionModel.findById(body.viewId),
        SnacksModel.findById(body.viewId)
    ])

    const view = matchingViews.find((e) => e != null)

    if (!view) {
        res.status(400)
        res.json({
            error: `Page with id ${body.viewId} doesn't exist`
        })
        res.end()
        return
    }

    view.totalViews += 1
    view.totalDurationOnPage += body.timeOnPage

    await view.save()

    res.status(201)
    res.end()
}
