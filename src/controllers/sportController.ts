import { rejects } from 'assert'
import { KeyObject } from 'crypto'
import e, { Request, Response } from 'express'

import { SportModel, SportSchema } from '../database/sport'
import logger from '../logger'

// post /sport  # adds a sport
/**
 * Expects at least name and url
 */
export let addSport = (req: Request, res: Response) => {
    // TODO: generate missing date, like categoryWeights etc.
    var Sport = new SportModel(req.body)

    Sport.save((err: any) => {
        if (err) {
            res.send(err)
        } else {
            res.send(Sport)
        }
    })
}

export const allSports = async (req: Request, res: Response) => {
    const allSports = await SportModel.find({}).exec()
    if (!allSports) {
        res.status(500)
        res.json({
            error: 'no sports found'
        })
    }

    res.status(200)
    res.json(allSports)
}

interface UpdateLists {
    toRemove: { name: string; url: string }[]
    toAdd: { name: string; url: string }[]
    toActive: { name: string; url: string }[]
    toInactive: { name: string; url: string }[]
}
export let updateSportsfromLists = async (req: Request, res: Response) => {
    const categoryWeights = new Map()

    const { toRemove, toAdd, toActive, toInactive } = req.body

    if (!toRemove || !toAdd || !toActive || !toInactive) {
        res.status(400)
        logger.info(req.body)
        res.json({
            error: 'missing parameters'
        })
        return
    }

    //fix if first entries in db
    try {
        let categoryWeightsDB = (await SportModel.findOne().lean().exec())
            ?.categoryWeights

        for (let category in categoryWeightsDB) {
            // {{sportart1:0} , {sportart2:0},{sportart3:1} , {sportart4:2},{sportart5:1} , {sportart6:2}}
            categoryWeights.set(category, 0)
        }
    } catch {
        console.log('no categorieWeights exist in the database so far')
    }

    // add
    for (const sport of toAdd) {
        // if list comes from synced with only 2 parameters / implies new Sports
        // only has URL and Name {{name: "vollebay" , url:"www.tets.de"},...}
        if (
            'name' in sport &&
            'url' in sport &&
            Object.keys(sport).length == 2
        ) {
            const DBsport = new SportModel({
                name: sport.name,
                url: sport.url,
                categoryWeights: categoryWeights,
                totalClicks: 0,
                totalRecommended: 0,
                active: false
            })
            await DBsport.save()
        } else {
            return res
                .status(400)
                .send(
                    'wrong format at' +
                        sport.name +
                        ', should be: {name: "test", url:"www.test.de"} all entriers prior have been added to the databse'
                )
        }
    }

    // active / inactive
    for (const sport of [
        ...toActive.map((e) => {
            return { ...e, active: true }
        }),
        ...toInactive.map((e) => {
            return { ...e, active: false }
        })
    ]) {
        // if list comes from synced with only 2 parameters / implies new Sports
        // only has URL and Name {{name: "vollebay" , url:"www.tets.de"},...}
        if ('name' in sport) {
            await SportModel.findOneAndUpdate(
                { name: sport.name },
                {
                    $set: {
                        name: sport.name,
                        url: sport.url,
                        categoryWeights: categoryWeights,
                        totalClicks: 0,
                        totalRecommended: 0,
                        active: sport.active
                    }
                },
                { sort: { name: 1 }, upsert: true, new: true }
            )
                .then(() => {
                    console.log(
                        'sport ' +
                            sport.name +
                            ' is now ' +
                            (sport.active ? 'active' : 'inactive')
                    )
                })
                .catch(() => {
                    return res.send(
                        'failed to add' + sport.name + 'to database'
                    )
                })
        } else {
            return res
                .status(400)
                .send(
                    'wrong format at' +
                        sport.name +
                        ', should be: {name: "test", url:"www.test.de"} all entriers prior have been added to the databse'
                )
        }
    }

    // remove
    for (const sport of toRemove) {
        if ('name' in sport) {
            await SportModel.findOneAndRemove({ name: sport.name }).then(() => {
                console.log('removed sport ' + sport.name)
            })
        } else {
            return res
                .status(400)
                .send(
                    'wrong format at' +
                        sport.name +
                        ', should be: {name: "test", url:"www.test.de"} all entriers prior have been added to the databse'
                )
        }
    }

    res.status(200).send('all Sports updated successfully')
}

//if list comes from SportsView with all parameters
