import { Request, Response } from 'express'
import { SnacksModel, SnacksSchema } from '../database/snack'

// post /snack  # adds a snack
export let addSnack = (req: Request, res: Response) => {
    var Snack = new SnacksModel(req.body)

    Snack.save((err: any) => {
        if (err) {
            res.send(err)
        } else {
            res.send(Snack)
        }
    })
}

// get /snacks  # returns all snacks
export let allSnacks = (req: Request, res: Response) => {
    let Snacks = SnacksModel.find((err: any, Snacks: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send(Snacks)
        }
    })
}

// get /snacks/{index} # returns snack with id index
export let getSnack = (req: Request, res: Response) => {
    SnacksModel.findById(req.params.id, (err: any, Snacks: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send(Snacks)
        }
    })
}

// delete /snack/{index}
export let deleteSnack = (req: Request, res: Response) => {
    SnacksModel.deleteOne({ _index: req.params.index }, (err: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send('success')
        }
    })
}

export let addSnacksfromList = async (req: Request, res: Response) => {
    //check if all entries are correct
    for (const snack of req.body) {
        if (
            !(
                '_id' in snack &&
                'active' in snack &&
                'imageUrl' in snack &&
                'title' in snack &&
                'totalDurationOnPage' in snack &&
                'totalViews' in snack &&
                'descriptions' in snack
            )
        ) {
            return res
                .status(400)
                .send(
                    'wrong format at' +
                        snack.title +
                        ', should be: {_id: "test", active: ...,imageurl: ..., title: ...,  totalDurationOnPage: ..., totalviews: ..., descriptions: ...}'
                )
        }
    }
    await SnacksModel.collection
        .drop()
        .then(() => console.log('Snacks gedropped resolved'))
        .catch(() => {
            console.log('Snacks table didnt exist in the first place')
        })

    for (const snack of req.body) {
        var Snack = new SnacksModel(snack)
        Snack.save((err: any) => {
            if (err) {
                res.send(err)
            }
        })
    }
    res.status(200).send('all Snacks added successfully')
}
