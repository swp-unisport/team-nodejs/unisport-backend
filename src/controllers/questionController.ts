import { Request, Response } from 'express'
import { QuestionModel } from '../database/question'

export let addAllQuestions = async (req: Request, res: Response) => {
    if (!Array.isArray(req.body)) {
        res.status(400)
        res.send('expected array of questions')
        return
    }

    await QuestionModel.collection.drop()

    for (const question of req.body) {
        await QuestionModel.findOneAndUpdate(
            { _id: question._id },
            {
                $set: {
                    descriptions: question.descriptions,
                    active: question.active,
                    category: question.category
                }
            },
            { upsert: true, new: true }
        ).catch(() => {
            return res.send(
                'failed to add' + question.descriptions['de'] + 'to database'
            )
        })
    }

    res.status(200)
    res.send('all questions updated')
}

// get /questions # returns all questions
export let allQuestions = (req: Request, res: Response) => {
    let question = QuestionModel.find((err: any, question: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send(question)
        }
    })
}

// get /question/{index} # returns question with id index
export let getQuestion = (req: Request, res: Response) => {
    QuestionModel.findById(req.params.id, (err: any, question: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send(question)
        }
    })
}

// delete /question/{index}
export let deleteQuestion = (req: Request, res: Response) => {
    QuestionModel.deleteOne({ _index: req.params.index }, (err: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send('success')
        }
    })
}
