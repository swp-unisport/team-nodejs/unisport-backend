import { rejects } from 'assert'
import { Request, Response } from 'express'
import { ActivityModel } from '../database/activity'

export let addActivity = (req: Request, res: Response) => {
    if (
        typeof req.body !== 'object' ||
        !('descriptions' in req.body) ||
        !('duration' in req.body) ||
        !('totalViews' in req.body) ||
        !('totalDurationOnPage' in req.body) ||
        !('descriptions' in req.body)
    ) {
        res.status(400).send('incompatible body for creating a activity')
        res.end()
    } else {
        var activity = new ActivityModel(req.body)

        activity.save((err: any) => {
            if (err) {
                res.send(err)
            } else {
                res.send(activity)
            }
        })
    }
}

// get /activity  # returns all activity
export let allActivity = (req: Request, res: Response) => {
    console.log(req.body)
    ActivityModel.find((err: any, activity: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send(activity)
        }
    })
}

// get /activity/{index} # returns ativty with id index
export let getActivity = (req: Request, res: Response) => {
    ActivityModel.findById(req.params.id, (err: any, activity: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send(activity)
        }
    })
}

// delete /snack/{index}
export let deleteSnack = (req: Request, res: Response) => {
    ActivityModel.deleteOne({ _index: req.params.index }, (err: any) => {
        if (err) {
            res.send('Error!')
        } else {
            res.send('success')
        }
    })
}

export let updateActivity = (req: Request, res: Response) => {
    let activity = ActivityModel.findByIdAndUpdate(
        req.params.id,
        req.body,
        (err: any, Activities: any) => {
            if (err) {
                res.send(err)
            } else {
                res.send('Successfully updated snack!')
            }
        }
    )
}

export let addActivitiesfromList = async (req: Request, res: Response) => {
    //check if all entries are correct
    for (const activity of req.body) {
        if (
            !(
                '_id' in activity &&
                'active' in activity &&
                'imageUrl' in activity &&
                'title' in activity &&
                'totalDurationOnPage' in activity &&
                'totalViews' in activity &&
                'descriptions' in activity
            )
        ) {
            return res
                .status(400)
                .send(
                    'wrong format at' +
                        activity.title +
                        ', should be: {_id: "test", active: ...,imageurl: ..., title: ...,  totalDurationOnPage: ..., totalviews: ..., descriptions: ...}'
                )
        }
    }
    await ActivityModel.collection
        .drop()
        .then(() => console.log('Activities gedropped resolved'))
        .catch(() => {
            console.log('Activities table didnt exist in the first place')
        })

    for (const snack of req.body) {
        var activity = new ActivityModel(snack)
        activity.save((err: any) => {
            if (err) {
                res.send(err)
            }
        })
    }
    res.status(200).send('all Snacks added successfully')
}
