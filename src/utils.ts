export async function returnSomeString(id: number): Promise<string> {
    return new Promise((resolve) => {
        setTimeout(() => resolve(`Hello world ${id}`), 1000)
    })
}

export function untestedFunction(): number {
    return 2 + 2
}

export function simpleSum(a: number, b: number) {
    if (a + b === 42) {
        return '42'
    }
    return a + b
}
