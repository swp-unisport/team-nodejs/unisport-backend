import mongoose, { Model, ObjectId } from 'mongoose'
export interface ISettings extends mongoose.Document<ObjectId> {
    message: string
    imageUrl: string
    sportListFetchUrl: string
    adminPasswordHash: string
}

// Note the capitalized `String`: those are js classes, not typescript types!
export const SettingsSchema = new mongoose.Schema({
    message: { type: String, required: true },
    imageUrl: { type: String, required: true },
    sportListFetchUrl: { type: String, required: true },
    adminPasswordHash: { type: String, required: true }
})

export const SettingsModel: Model<ISettings> = mongoose.model(
    'Settings',
    SettingsSchema
)
