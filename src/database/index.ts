import mongoose from 'mongoose'
import logger from '../logger'
import { ActivityModel } from './activity'
import { OrderedModel } from './ordered'
import { QuestionModel } from './question'
import { SnacksModel } from './snack'

const uri: string = process.env.MONGO_URL || 'mongodb://127.0.0.1:27017/local'

export async function connectMongoDB(): Promise<typeof mongoose> {
    logger.info('Connecting to MongoDB with url ' + uri)

    return mongoose
        .connect(uri, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        .then((mongo) => {
            logger.info('MongoDB Connnected')
            return mongo
        })
}

export async function generateDefaultOrder() {
    const existingOrder = await OrderedModel.findOne({}).exec()
    if (existingOrder) {
        return
    }
    logger.info(
        'No default order was found, generating one from existing pages'
    )

    const Snacks = await SnacksModel.find({ active: true }).exec()
    const sIds = (Snacks ?? []).map((s) => s._id)
    const Questions = await QuestionModel.find({ active: true }).exec()
    const qIds = (Questions ?? []).map((s) => s._id)
    let Activities = await ActivityModel.find({ active: true }).exec()
    const aIds = (Activities ?? []).map((s) => s._id)
    let AllActivePages = [...sIds, ...qIds, ...aIds]

    // randomize order
    for (let i = AllActivePages.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1))
        ;[AllActivePages[i], AllActivePages[j]] = [
            AllActivePages[j],
            AllActivePages[i]
        ]
    }

    const defaultOrder = new OrderedModel({ order: AllActivePages })
    await defaultOrder.save()
    logger.info('created random default order')
}
