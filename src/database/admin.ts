import mongoose, { Model, ObjectId } from 'mongoose'
export interface IAdmin extends mongoose.Document<ObjectId> {
    username: string
    password: string
    active: boolean
}

// Note the capitalized `String`: those are js classes, not typescript types!
export const AdminSchema = new mongoose.Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    active: { type: Boolean, required: true }
})

export const AdminModel: Model<IAdmin> = mongoose.model('Admin', AdminSchema)
