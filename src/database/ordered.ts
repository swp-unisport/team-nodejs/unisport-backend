import mongoose, { Model, ObjectId } from 'mongoose'
export interface IOrdered extends mongoose.Document<ObjectId> {
    order: ObjectId[]
}

// Note the capitalized `String`: those are js classes, not typescript types!
export const OrderedSchema = new mongoose.Schema({
    order: { type: Array, required: true }
})

export const OrderedModel: Model<IOrdered> = mongoose.model(
    'Ordered',
    OrderedSchema
)

// wenn neues erstellt wird wird es appended array
// auf der seite reihenfolge kann das mit drag and drop tauschen
// frontEndcontainer queried die liste der Objects
