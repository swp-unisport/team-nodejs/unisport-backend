import mongoose, { Model, ObjectId } from 'mongoose'
export interface ISports extends mongoose.Document<ObjectId> {
    name: string
    categoryWeights: Map<string, number | undefined>
    url: string
    totalClicks: number
    totalRecommended: number
    active: boolean
}

// Note the capitalized `String`: those are js classes, not typescript types!
export const SportSchema = new mongoose.Schema({
    name: { type: String, required: true },
    categoryWeights: { type: Map, of: Number },
    url: { type: String, required: true },
    totalClicks: { type: Number, required: true },
    totalRecommended: { type: Number, required: true },
    active: { type: Boolean, required: true }
})

export const SportModel: Model<ISports> = mongoose.model('Sport', SportSchema)
