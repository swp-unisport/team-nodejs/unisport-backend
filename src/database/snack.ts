import mongoose, { Model, ObjectId } from 'mongoose'
import { TranslatedString } from '../types/ApiTypes'
export interface ISnacks extends mongoose.Document<ObjectId> {
    title: string
    imageUrl: string
    descriptions: TranslatedString // {de: "bsp", en: "ex.", fr:"..."}
    active: boolean
    totalViews: number
    totalDurationOnPage: number
}

// Note the capitalized `String`: those are js classes, not typescript types!
export const SnacksSchema = new mongoose.Schema({
    imageUrl: { type: String, required: true },
    descriptions: { type: Map, of: String, required: true },
    active: { type: Boolean, required: true },
    totalViews: { type: Number, required: true },
    totalDurationOnPage: { type: Number, required: true },
    title: { type: String, required: true }
})

export const SnacksModel: Model<ISnacks> = mongoose.model(
    'Snacks',
    SnacksSchema
)
