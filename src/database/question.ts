import mongoose, { Model, ObjectId } from 'mongoose'
import { TranslatedString } from '../types/ApiTypes'
export interface IQuestion extends mongoose.Document<ObjectId> {
    descriptions: TranslatedString // {de: "bsp", en: "ex.", fr:"..."}
    active: boolean
    category: string
    totalViews: number
    totalDurationOnPage: number
}

// Note the capitalized `String`: those are js classes, not typescript types!
export const QuestionSchema = new mongoose.Schema({
    descriptions: { type: Map, of: String, required: true },
    active: { type: Boolean, required: true },
    category: { type: String, required: true },
    totalViews: { type: Number, required: true },
    totalDurationOnPage: { type: Number, required: true }
})

export const QuestionModel: Model<IQuestion> = mongoose.model(
    'Question',
    QuestionSchema
)
