import mongoose, { Model, ObjectId } from 'mongoose'
import { TranslatedString } from '../types/ApiTypes'
export interface IActivity extends mongoose.Document<ObjectId> {
    title: string
    active: boolean
    descriptions: TranslatedString // {de: "bsp", en: "ex.", fr:"..."}
    duration: number
    totalViews: number
    totalDurationOnPage: number
    imageUrl: string
}

// Note the capitalized `String`: those are js classes, not typescript types!
export const ActivitySchema = new mongoose.Schema({
    title: { type: String, required: true },
    active: { type: Boolean, required: true },
    descriptions: { type: Map, of: String, required: true },
    duration: { type: Number, required: true },
    totalViews: { type: Number, required: true },
    totalDurationOnPage: { type: Number, required: true },
    imageUrl: { type: String, required: false }
})

export const ActivityModel: Model<IActivity> = mongoose.model(
    'Activity',
    ActivitySchema
)
