import mongoose, { Model, ObjectId } from 'mongoose'

export type ImageType = 'png' | 'jpg' | 'svg' | 'gif'

export const MimeToImageType: { [mime: string]: ImageType } = {
    'image/jpeg': 'jpg',
    'image/png': 'png',
    'image/svg+xml': 'svg',
    'image/gif': 'gif'
}

export const AcceptImageHeader = Object.keys(MimeToImageType).join(', ')

export const ImageTypeToMime: { [type in ImageType]: string } = {
    jpg: 'image/jpeg',
    gif: 'image/gif',
    png: 'image/png',
    svg: 'image/svg+xml'
}

export interface IImage extends mongoose.Document<ObjectId> {
    data: Buffer
    imgType: ImageType
    uploadDate: Date
    metaData?: any
}

// Note the capitalized `String`: those are js classes, not typescript types!
export const ImageSchema = new mongoose.Schema({
    data: { type: Buffer, required: true },
    imgType: { type: String, required: true },
    uploadDate: { type: Date, required: true },
    metaData: { type: Object, required: false }
})

export const ImageModel: Model<IImage> = mongoose.model('Image', ImageSchema)

export function toUrl(img: IImage): string {
    return `/api/image/${img._id}.${img.imgType}`
}
