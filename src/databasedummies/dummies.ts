import { ISnacks, SnacksModel } from '../database/snack'
import { QuestionModel } from '../database/question'
import { ActivityModel } from '../database/activity'
import { ImageModel } from '../database/image'
import { http, https } from 'follow-redirects'
import { SportModel } from '../database/sport'
import { WSAEWOULDBLOCK } from 'constants'
import { rejects } from 'assert'
import { OrderedModel } from '../database/ordered'
const fs = require('fs')

// can be used to fill local databse with dummies for testing

let cleanDB = async () => {
    await SportModel.collection
        .drop()
        .then(() => console.log('Sportarten gedropped resolved'))
        .catch(() => {
            console.log('Sportarten table didnt exist in the first place')
        })
    await SnacksModel.collection
        .drop()
        .then(() => console.log('Snacks gedropped resolved'))
        .catch(() => {
            console.log('Snacks table didnt exist in the first place')
        })
    await QuestionModel.collection
        .drop()
        .then(() => console.log('Questions gedropped resolved'))
        .catch(() => {
            console.log('Questions table didnt exist in the first place')
        })
    await ActivityModel.collection
        .drop()
        .then(() => console.log('Activities gedropped resolved'))
        .catch(() => {
            console.log('Activites table didnt exist in the first place')
        })
    await ImageModel.collection
        .drop()
        .then(() => console.log('Image gedropped resolved'))
        .catch(() => {
            console.log('Image table didnt exist in the first place')
        })
    await OrderedModel.collection
        .drop()
        .then(() => console.log('Ordered gedropped resolved'))
        .catch(() => {
            console.log('Ordered table didnt exist in the first place')
        })
}

let PostRequest = async (data, path) => {
    const options = {
        method: 'POST',
        hostname: 'localhost',
        port: 5000,
        path: path,
        headers: {
            'Content-Type': 'application/json'
        },
        maxRedirects: 20
    }
    return new Promise<void>((resolve, rej) => {
        const req = http.request(options, (res) => resolve())

        req.on('error', (e) => {
            console.error(`problem with request: ${e.message}`)
            rej(e)
        })
        req.write(data)
        req.end()
    })
}

let GetRequest = async (res2) => {
    const options = {
        method: 'GET',
        hostname: 'localhost',
        port: 5000,
        path: '/api/activity'
    }

    const req = http.request(options, (res) => {
        console.log(`statusCode: ${res.statusCode}`)

        res.on('data', (d) => {
            const parsed = JSON.parse(d)

            for (let i = 0; i < parsed.length; i++) {
                PutRequest(
                    JSON.stringify({
                        descriptions: {
                            de: 'deTestactivity' + i,
                            en: 'enTestactivity' + i,
                            fr: 'frTestactivity' + i
                        },
                        duration: 15,
                        totalViews: Math.round(Math.random() * 1000),
                        totalDurationOnPage: Math.round(Math.random() * 10),
                        imageUrl: JSON.parse(res2).url
                    }),
                    '/api/updateactivities/' + parsed[i]._id
                )
            }
        })
    })

    req.on('error', (error) => {
        console.error(error)
    })

    req.end()
}

let PutRequest = async (data, path) => {
    const options = {
        method: 'POST',
        hostname: 'localhost',
        port: 5000,
        path: path,
        headers: {
            'Content-Type': 'application/json'
        },
        maxRedirects: 20
    }
    const req = http.request(options, (res) => {})

    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`)
    })
    req.write(data)
    req.end()
}

let PostRequestImage = async (data, path: string) => {
    const options = {
        method: 'POST',
        hostname: 'localhost',
        port: 5000,
        path: path,
        headers: {
            'Content-Type': 'image/png'
        },
        maxRedirects: 20
    }

    const req = await http.request(options, (res) => {
        res.on('data', async (res) => {
            JSON.parse(res).url

            await GetRequest(res)
        })

        res.on('error', function (error) {
            console.error(error)
        })
    })

    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`)
    })
    req.write(data)
    req.end()
}

let questionsEinfügen = async () => {
    let i: number

    const questions = [
        {
            descriptions: {
                de: 'Willst du Sport mit einem Ball machen?',
                en: 'Would you like to train with a ball',
                fr: 'dfsd sdf sdfs dfds'
            },
            active: true,
            index: 1,
            category: 'Kampfsport',
            totalViews: 200,
            totalDurationOnPage: 5
        }
    ]

    questions.push({
        descriptions: {
            de: 'Willst du mehr als 2x die Woche Sport treiben?',
            en: 'Would you like to train more than to times a week?',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        index: 2,
        category: 'kategorie1',
        totalViews: 250,
        totalDurationOnPage: 4
    })
    questions.push({
        descriptions: {
            de: 'Willst du beim Sport Nass werden?',
            en: 'Would you like to get wet while practicing?',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        index: 2,
        category: 'kategorie2',
        totalViews: 250,
        totalDurationOnPage: 4
    })
    questions.push({
        descriptions: {
            de: 'Willst du Einzelsport treiben?',
            en: 'Would you like to train by yourself?',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        index: 2,
        category: 'kategorie5',
        totalViews: 250,
        totalDurationOnPage: 4
    })
    questions.push({
        descriptions: {
            de: 'Willst du Teamsport treiben?',
            en: 'Would you like to train in a group?',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        index: 2,
        category: 'kategorie5',
        totalViews: 400,
        totalDurationOnPage: 2
    })
    questions.push({
        descriptions: {
            de: 'Willst du am Abend Sport machen?',
            en: 'Would you like to train in the afternoon?',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        index: 2,
        category: 'kategorie9',
        totalViews: 400,
        totalDurationOnPage: 2
    })
    questions.push({
        descriptions: {
            de: 'Willst du draußen Sport treiben?',
            en: 'Would you like to train outside?',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        index: 2,
        category: 'kategorie9',
        totalViews: 400,
        totalDurationOnPage: 2
    })

    await PostRequest(JSON.stringify(questions), '/api/question')
        .then(() => {
            console.log('added Questions to Database')
        })
        .catch(() => {
            console.log('failed to add Questions to Database')
        })
}

let snacksEinfügen = async () => {
    let i: number
    // dummies Snack
    const snacks = [
        {
            title: 'Lol important Snack',
            imageUrl: 'http://placekitten.com/300/300',
            descriptions: {
                de: 'Wusstest du.... Salat schrumpft den Bizeps',
                en: 'Did you know... Salad shrinks the biceps',
                fr: 'dfsd sdf sdfs dfds'
            },
            active: true,
            totalViews: 2500,
            totalDurationOnPage: 12
        }
    ]

    snacks.push({
        title: 'Snack #1',
        imageUrl: 'http://placekitten.com/300/300',
        descriptions: {
            de: 'Wusstest du.... test2',
            en: 'Did you know... test2',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        totalViews: 250,
        totalDurationOnPage: 6
    })

    snacks.push({
        title: 'Snack #2',
        imageUrl: 'http://placekitten.com/300/300',
        descriptions: {
            de: 'Wusstest du.... test3',
            en: 'Did you know... test3',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        totalViews: 120,
        totalDurationOnPage: 4
    })

    snacks.push({
        title: 'Snack #3',
        imageUrl: 'http://placekitten.com/300/300',
        descriptions: {
            de: 'Wusstest du.... test4',
            en: 'Did you know... test4',
            fr: 'dfsd sdf sdfs dfds'
        },
        active: true,
        totalViews: 600,
        totalDurationOnPage: 4
    })

    for (i = 0; i < snacks.length; i++) {
        await PostRequest(JSON.stringify(snacks[i]), '/api/snack')
            .then(() => {
                console.log('added Snack ' + i + ' to Database')
            })
            .catch(() => {
                console.log('failed to add Snack ' + i + ' to Database')
            })
    }
}

let activitiesEinfügen = async () => {
    let i: number
    const activities = [
        {
            title: 'activity1',
            active: true,
            descriptions: {
                de: 'Kreise deine Arme rückwärts',
                en: 'circle your arms backwards',
                fr: 'dfsd sdf sdfs dfds'
            },
            duration: 10,
            totalViews: 200,
            totalDurationOnPage: 5,
            imageUrl: 'test.ww'
        }
    ]

    activities.push({
        title: 'activity2',
        active: true,
        descriptions: {
            de: 'Kreise deine Beine rückwärts',
            en: 'circle your legs backwards',
            fr: 'dfsd sdf sdfs dfds'
        },
        duration: 10,
        totalViews: 200,
        totalDurationOnPage: 5,
        imageUrl: 'test.ww'
    })

    activities.push({
        title: 'activity3',
        active: true,
        descriptions: {
            de: 'Kreise deine Schulter rückwärts',
            en: 'circle your shoulder backwards',
            fr: 'dfsd sdf sdfs dfds'
        },
        duration: 10,
        totalViews: 2400,
        totalDurationOnPage: 5,
        imageUrl: 'test.ww'
    })

    activities.push({
        title: 'activity4',
        active: true,
        descriptions: {
            de: 'Kreise deine Nacken rückwärts',
            en: 'circle your neck backwards',
            fr: 'dfsd sdf sdfs dfds'
        },
        duration: 15,
        totalViews: 900,
        totalDurationOnPage: 2,
        imageUrl: 'test.ww'
    })

    for (i = 0; i < activities.length; i++) {
        await PostRequest(JSON.stringify(activities[i]), '/api/activity')
            .then(() => {
                console.log('added Snack ' + i + ' to Database')
            })
            .catch(() => {
                console.log('failed to add Snack ' + i + ' to Database')
            })
    }
}

let SportartenEinfügen = async () => {
    let i: number

    const sports = [
        {
            name: 'Ballett - Jazzdance Combo (EN) @ Home',
            categoryWeights: {
                kategorie1: Math.round(Math.random() * 10),
                kategorie2: Math.round(Math.random() * 10),
                kategorie3: Math.round(Math.random() * 10),
                kategorie4: Math.round(Math.random() * 10),
                kategorie5: Math.round(Math.random() * 10),
                kategorie6: Math.round(Math.random() * 10),
                kategorie7: Math.round(Math.random() * 10),
                kategorie8: Math.round(Math.random() * 10),
                kategorie9: Math.round(Math.random() * 10),
                kategorie10: Math.round(Math.random() * 10)
            },
            url:
                'www.buchsys.de/fu-berlin/angebote/aktueller_zeitraum/_Ballett_-_Jazzdance_Combo__EN____HOME.html',
            active: Math.random() < 0.5,
            totalClicks: 100,
            totalRecommended: 240
        }
    ]

    for (i = 0; i < 150; i++) {
        let x = Math.round(Math.random() * 250)
        sports.push({
            name: 'Sportart' + i,
            categoryWeights: {
                kategorie1: Math.round(Math.random() * 10),
                kategorie2: Math.round(Math.random() * 10),
                kategorie3: Math.round(Math.random() * 10),
                kategorie4: Math.round(Math.random() * 10),
                kategorie5: Math.round(Math.random() * 10),
                kategorie6: Math.round(Math.random() * 10),
                kategorie7: Math.round(Math.random() * 10),
                kategorie8: Math.round(Math.random() * 10),
                kategorie9: Math.round(Math.random() * 10),
                kategorie10: Math.round(Math.random() * 10)
            },
            url:
                'www.buchsys.de/fu-berlin/angebote/aktueller_zeitraum/_Ballett_-_Jazzdance_Combo__EN____HOME.html',
            active: Math.random() < 0.5,
            totalClicks: Math.round(Math.random() * x),
            totalRecommended: x
        })
    }

    for (i = 0; i < sports.length; i++) {
        await PostRequest(JSON.stringify(sports[i]), '/api/sport')
            .then(() => {
                console.log('added sport ' + i + ' to Database')
            })
            .catch(() => {
                console.log('failed to add sport ' + i + ' to Database')
            })
    }
}

let imagesEinfügen = async () => {
    const data = fs.readFileSync('./src/databasedummies/test.png')
    console.log('sind wir on time?')
    await PostRequestImage(data, '/api/image')
}

export let isDummyGeneratorActive = false

export let createDummieDatabase = async () => {
    isDummyGeneratorActive = true
    await cleanDB()
    await questionsEinfügen()
    await snacksEinfügen()
    await activitiesEinfügen()
    await SportartenEinfügen()
    await imagesEinfügen()
    // wait a bit to enable authentification
    setTimeout(() => (isDummyGeneratorActive = false), 200)
}
