import { returnSomeString, untestedFunction, simpleSum } from './utils'

test('return the right Promise<string>', async () => {
    jest.useFakeTimers()
    const promise = returnSomeString(42)
    jest.runAllTimers()
    expect(await promise).toBe('Hello world 42')
})

test('untested fucntion test', () => {
    expect(untestedFunction()).toBe(4)
})

test('properly adds two number', () => {
    expect(simpleSum(5, 5)).toEqual(10)
})
