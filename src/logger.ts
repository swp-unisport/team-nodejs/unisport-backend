import winston from 'winston'

const logFormat = winston.format.printf(({ level, message }) => {
    return `${new Date().toISOString()}-${level}: ${JSON.stringify(
        message,
        null,
        4
    )}\n`
})

const logger = winston.createLogger({
    transports: [
        new winston.transports.Console({
            level: 'debug',
            format: winston.format.combine(winston.format.colorize(), logFormat)
        })
    ]
})

export default logger
