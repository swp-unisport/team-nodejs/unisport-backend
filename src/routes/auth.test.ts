import { AdminModel, IAdmin } from '../database/admin'
import { signinController, signupController, validateToken } from './auth'
import jsonwebtoken from 'jsonwebtoken'

class MockResponse {
    status = jest.fn(() => this)
    json = jest.fn((payload: object) => this)
}

describe('Authentification', () => {
    beforeAll(() => {
        process.env.TOKEN_SECRET = 'veryUnsafeSecret'
    })

    describe('signup', () => {
        it('should save new admin on signup', async () => {
            AdminModel.prototype.save = jest.fn()
            const mockRes = new MockResponse()
            await signupController(
                {
                    body: {
                        password: 'hello',
                        username: 'Peter'
                    }
                } as any,
                mockRes as any
            )

            expect(mockRes.status).toBeCalledWith(201)
            expect(AdminModel.prototype.save).toBeCalled()
        })

        it('should fail for missing passowrd', async () => {
            AdminModel.prototype.save = jest.fn()
            const mockRes = new MockResponse()
            await signupController(
                {
                    body: {
                        username: 'Peter'
                    }
                } as any,
                mockRes as any
            )

            expect(mockRes.status).toBeCalledWith(400)
            expect(AdminModel.prototype.save).not.toBeCalled()
        })
    })

    describe('signin', () => {
        it('should fail for missing password', async () => {
            const mockRes = new MockResponse()
            await signinController(
                {
                    body: {
                        username: 'Peter'
                    }
                } as any,
                mockRes as any
            )

            expect(mockRes.status).toBeCalledWith(400)
        })

        it('should work for registered admin', async () => {
            let newAdmin: IAdmin = null
            AdminModel.prototype.save = function () {
                newAdmin = this
            }
            let mockRes = new MockResponse()

            // store new admin
            await signupController(
                {
                    body: {
                        username: 'Peter',
                        password: '1234asdf'
                    }
                } as any,
                mockRes as any
            )

            expect(mockRes.status).toBeCalledWith(201)
            expect(newAdmin).toBeDefined()

            // test signin with same credentials

            // mock findOne to return just stored admin
            AdminModel.findOne = jest.fn(({ username }) => {
                return {
                    async exec() {
                        if (username === newAdmin.username) {
                            return newAdmin
                        }
                        return null
                    }
                }
            }) as any

            mockRes = new MockResponse() // reset status

            await signinController(
                {
                    body: {
                        password: '1234asdf',
                        username: 'Peter'
                    }
                } as any,
                mockRes as any
            )

            expect(mockRes.status).toHaveBeenCalledWith(200)

            const jwt = mockRes.json.mock.calls[0][0] as any
            expect(jwt.expiresIn).toBe(3600)

            // test it jwt is valid
            const tokenPayload = validateToken(jwt.token)
            expect(tokenPayload.username).toBe('Peter')
        })
    })
})
