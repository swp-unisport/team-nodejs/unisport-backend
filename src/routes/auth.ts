import * as bcrypt from 'bcrypt'
import * as jwt from 'jsonwebtoken'
import { Request, Response } from 'express'

import { AdminModel, IAdmin } from '../database/admin'
import logger from '../logger'

/**
 *
 * @param token the jwt passed to the server
 * @returns the payload of the jwt if valid, else throws error
 * @throws on invalid token
 */
export function validateToken(token: string): { username: string } {
    return jwt.verify(token, process.env.TOKEN_SECRET) as any
}

/**
 * @swagger
 * /api/auth/signup:
 *  post:
 *    description: Register admin account
 *    parameters:
 *      - name: username
 *        description: Admin's username.
 *        required: true
 *        type: string
 *      - name: password
 *        description: Admin's password.
 *        required: true
 *        type: string
 *    responses:
 *      '201':
 *        description: A successful response
 *      '500':
 *        description: Error
 */
export const signupController = async (req: Request, res: Response) => {
    if (!req.body.username || !req.body.password) {
        res.status(400).json({
            error: 'password or username missing'
        })
        return
    }

    const hash = await bcrypt.hash(req.body.password, 10)
    const admin = new AdminModel({
        username: req.body.username,
        password: hash,
        active: true
    })

    try {
        await admin.save()
        res.status(201).json({
            message: 'Erfolgreich Registrierung!'
        })
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
}

/**
 * @swagger
 * /api/auth/signin:
 *  post:
 *    description: Login to admin account
 *    parameters:
 *      - name: username
 *        description: Admin's username.
 *        required: true
 *        type: string
 *      - name: password
 *        description: Admin's password.
 *        required: true
 *        type: string
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Login failed
 */
export const signinController = async (req: Request, res: Response) => {
    if (!req.body.username || !req.body.password) {
        res.status(400).json({ error: 'missing passowrd or username' })
        return
    }

    let fetchedAdmin: IAdmin = await AdminModel.findOne({
        username: req.body.username
    }).exec()

    if (!fetchedAdmin) {
        return res.status(401).json({
            message: 'Admin mit diesem Nutzernamen existiert nicht!.'
        })
    }

    if (!fetchedAdmin.active) {
        return res.status(401).json({
            message: 'Admin mit diesem Nutzernamen ist inaktiv!.'
        })
    }

    await bcrypt
        .compare(req.body.password, fetchedAdmin.password)
        .then((passwordValid) => {
            if (!passwordValid) {
                return res.status(401).json({
                    message: 'Falsches Passwort!'
                })
            }

            const token = jwt.sign(
                { username: fetchedAdmin.username },
                process.env.TOKEN_SECRET,
                { expiresIn: '1h' }
            )

            res.status(200).json({
                token: token,
                expiresIn: 3600
            })
        })
}
