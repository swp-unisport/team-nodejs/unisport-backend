import * as express from 'express'
import { ISports, SportModel } from '../database/sport'

import logger from '../logger'

interface UserAnswersBody {
    answers: { category: string; answer: number; weight: number }[]
}

function validateUserAnswersBody(
    reqBody: UserAnswersBody
): reqBody is UserAnswersBody {
    console.log('req body', reqBody)
    if (typeof reqBody !== 'object' || !Array.isArray(reqBody.answers))
        return false
    for (const answer of reqBody.answers) {
        if (
            typeof answer !== 'object' ||
            typeof answer.category !== 'string' ||
            typeof answer.answer !== 'number' ||
            typeof answer.weight !== 'number'
        )
            return false
    }
    return true
}

/**
 * @swagger
 * /api/get-matching-sports:
 *  post:
 *    description: Calculate the most matching sports based on selected answers and weights
 *    requestBody:
 *      required: true
 *      description: The answered questions (answer-score + weight) + user metadata?
 *      content:
 *        'application/json':
 *          schema:
 *            type: object
 *            properties:
 *              answers:
 *                type: array
 *                items:
 *                  type: object
 *                  properties:
 *                    category:
 *                      type: string
 *                      description: the category to which the question belongs
 *                    answer:
 *                      type: number
 *                      description: selected with the radio buttons
 *                    weight:
 *                      type: number
 *                      description: selected with the slider - how important this question was
 *    responses:
 *      '201':
 *        description: A successful response
 *      '500':
 *        description: Error
 */
export const calcMatchingSportsController = async (req, res) => {
    if (!validateUserAnswersBody(req.body)) {
        res.status(400)
        res.end()
        return
    }

    const allSportsPromise = SportModel.find({ active: true }).exec()

    const userBody = req.body as UserAnswersBody

    let scorePerCategory: { [category: string]: number } = {}

    for (const answer of userBody.answers) {
        if (!scorePerCategory[answer.category])
            scorePerCategory[answer.category] = 0

        scorePerCategory[answer.category] += answer.answer * answer.weight
    }

    console.log(scorePerCategory)

    const allSports = await allSportsPromise

    // maps a sport to its total score
    const scorePerSport: [number, ISports][] = []

    let maxScore = 1 // to prevent dividing by zero
    // how the score per sport is calculated:
    // sum of each category factor * user's scorePerCategory
    for (const sport of allSports) {
        let score = 0
        for (const [cat, factor] of sport.categoryWeights.entries()) {
            score += factor * (scorePerCategory[cat] || 0)
        }
        maxScore = Math.max(maxScore, score)
        scorePerSport.push([score, sport])
    }
    // console.log(scorePerSport.slice(0, 3))
    // sort the sport based on their score (TODO: check if sorting is the wrong way)
    scorePerSport.sort(([score1, _sport1], [score2, _sport2]) => {
        return score2 - score1
    })
    // TODO: maybe add more metadata to sent sport object?
    const responseArray = scorePerSport.slice(0, 10).map((sportEntry) => {
        return {
            name: sportEntry[1].name,
            score: sportEntry[0] / maxScore,
            url: sportEntry[1].url
        }
    })

    res.json(responseArray)
    res.end()
}
