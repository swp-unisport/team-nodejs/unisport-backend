import fetch from 'node-fetch'
import * as parser from 'node-html-parser'
export const DEFAULT_URL =
    'https://www.buchsys.de/fu-berlin/angebote/aktueller_zeitraum/index.html'

/**
 *
 * @returns [text, url][] of all sports listed under the url
 */
export async function parseFromBuchsys(
    url: string
): Promise<[string, string][]> {
    const res = await fetch(url)
    if (res.status !== 200) {
        throw new Error(`Failed to fetch ${url}: ${res.status}`)
    }

    // matches the last slash or .../asdasdd.htm(l)
    const lastHtmlRegex = /\/((\w|\d)+.html?)?$/
    // don't include last index.html or whatever in absolute path for links
    url = url.replace(lastHtmlRegex, '')

    const domain = new URL(url).origin

    const html = await res.text()

    const data = parser.parse(html).querySelector('dl').querySelectorAll('a')

    return data.map((child) => {
        let link = child.getAttribute('href')

        if (!link.startsWith('http://') && !link.startsWith('https://')) {
            // make absolute path
            if (link.startsWith('/')) {
                // should be relative to root of domain
                link = domain + '/' + link
            } else {
                link = url + '/' + link
            }
        }

        return [child.text, link]
    })
}
