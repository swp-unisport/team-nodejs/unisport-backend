# unisport-backend

Node.js with express and Typescript based backend server

## backend technology stack

-   runtime: [node js](https://nodejs.org/en/)
-   language: [typescript](https://www.typescriptlang.org/), compiles to Javascript (common-js module system) in the ./dist folder
-   server-framework: [express](https://expressjs.com/de/) + [@types/express](https://www.npmjs.com/package/@types/express)
-   lint: [eslint](https://eslint.org/) with [standard-js styleguide](https://standardjs.com/) and @typescript-eslint
    -   **TODO:** do we want to use standard-js or for example google or airbnb as style rules?
    -   config is in [.eslitrc.js](./.eslintrc.js) and [.eslintignore](./.eslintignore)
-   test: [jest](https://jestjs.io/) + ts-jest, also for coverage
    -   Gitlab CICD pipline requires **80% coverage** to succeed

## TODOS

-   lint styleguide: see above
-   configure CICD pipes to cache node_modules
-   is 80% coverage Ok?

## commands to execute:

### linting

-   `npm run lint`
-   `npm run lint-and-fix`

uses eslint with the config from [./.eslintrc](), and fixes errors when using the ...-and_fix command.

### testing

`npm run test`

runs [jest](https://jestjs.io) on all \*.test.ts files in ./src

### executing

`npm run start`

compiles all ./src files (except \*.test.ts) to ./dist and executes ./dist/app.js with node

### building

`npm run build`

compiles all ./src files (except \*.test.ts) to ./dist
