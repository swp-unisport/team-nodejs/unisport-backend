# App deployment

## tested software versions

-   node: v14.7.0
-   mongodb community: 4.4.6

## env vars to set

All env vars cen be set in an .env file in the root directoy of the project or just via shell

-   NODE_ENV='production'
-   MONGO_URL='mongodb://127.0.0.1:27017/local' or whereever your mongo db instance is running
-   TOKEN_SECRET='...jwt secret'
-   APP_PORT='1234' or whereever yo want, default is 5000

Our application uses React JS with Typescript on frontend and Node JS with Typescript and Express with MongoDB on backend.
In order to reduce number of servers needed to deploy an app and in order to avoid CORS, we suggest deploying an app by serving the React part from our Express server.
Here are the steps which need to be followed to have app deployment-ready and served directly from the Express server:

1. In our frontend folder, we run command `npm run build`, that creates a "build" folder.

2. We copy the build folder contents and paste it into a new backend folder `./frontend-build`.

3. run `npm run deploy` from backend root folder

    - this will build ts project
    - copies bundle into `./deploy` folder as well as `package.json` and `package-lock.json`,
    - installs npm runtime dependencies into that folders node_modules
    - creates a zip in the root containing all deploy files

4. when extracted on target system, specify env vars (easiest via .env file) and run `node ./dist/app.js`

## create first admin

To generate a first admin on startup, you can insert one like that: `node ./dist/app.js --create-admin Detlev asdf1234`

## create more admins from browser

When logged in, you can create new admins with this js snipped (paste into console)

```
fetch('/api/auth/signup', {
  body: JSON.stringify({
    username: "Kai",
    password: "super123"
  }),
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    Authentification: `Bearer ${localStorage.getItem('token')}`
  }
}).then(e => e.json()).then(console.log)
```
