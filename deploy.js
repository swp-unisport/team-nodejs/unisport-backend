/**
 * == Node script for backend deployment ==
 *
 * Expects current build in the `dist` folder
 * this places all relevant data for production run in the `deploy` folder and installs dependencies
 */

const { execSync } = require('child_process')
const fs = require('fs')
//const fsp = require('fs/promises')
const fse = require('fs-extra')
const archiver = require('archiver')

console.log('copying relevant data to `./deploy`')

if (!fs.existsSync('./dist/app.js')) {
    console.log('execute this script only with npm run deploy')
    process.exit(-1)
}

let frontendBundle = true
if (!fs.existsSync('./frontend-build')) {
    frontendBundle = false
    console.warn(
        "The folder ./fronten-build doesn't exist. To have a monolithic app, build frontend with npm run build and copy contents of ./build into the folder ./frontend-build !"
    )
}

if (fs.existsSync('./deploy')) {
    fs.rmdirSync('./deploy', { recursive: true })
}

fs.mkdirSync('./deploy')
// create empty env file
const envFile = fs.openSync('./deploy/.env', 'w')
fs.closeSync(envFile)

fse.copySync('./dist', './deploy/dist')
fs.copyFileSync('./package.json', './deploy/package.json')
fs.copyFileSync('./package-lock.json', './deploy/package-lock.json')

if (frontendBundle) {
    fse.copySync('./frontend-build', './deploy/frontend-build')
}

process.chdir('./deploy')
console.log('installing runtime dependencies')
execSync('npm install --production')

process.chdir('..')

const d = new Date()
const ds = `${d.getFullYear()}_${
    d.getMonth() + 1
}_${d.getDate()}__${d.getHours()}_${d.getMinutes()}_${d.getSeconds()}`

console.log(`Creating zip ./deploy_${ds}.zip`)

const zipStream = fs.createWriteStream(`./deploy_${ds}.zip`)

const archive = archiver('zip', {
    zlib: { level: 9 } // Sets the compression level.
})

// This event is fired when the data source is drained no matter what was the data source.
// It is not part of this library but rather from the NodeJS Stream API.
// @see: https://nodejs.org/api/stream.html#stream_event_end
zipStream.on('end', function () {
    console.log('Data has been drained')
})

// good practice to catch warnings (ie stat failures and other non-blocking errors)
archive.on('warning', function (err) {
    if (err.code === 'ENOENT') {
        // log warning
    } else {
        // throw error
        throw err
    }
})

// good practice to catch this error explicitly
archive.on('error', function (err) {
    throw err
})

// pipe archive data to the file
archive.pipe(zipStream)

// write whole deploy folder into zip
archive.directory('./deploy', false)

archive
    .finalize()
    .then((r) =>
        console.log('finished zip creation in file ' + `./deploy_${ds}.zip`, r)
    )
